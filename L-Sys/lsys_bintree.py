import turtle
def axiom():
    return "0"

def rules(str):
    if str == "1":
        return "11"
    elif str == "0":
        return "1[0]0"
    else:
        return str

def next_gen(sys):
    next_gen = ""
    for cel in sys:
        next_gen += rules(cel)
    return next_gen

def lsys(axiom,rules,n):
    sys = axiom()
    for i in range(n):
        sys = next_gen(sys)
    return sys


def dessin(n,angle,l):
    turtle.reset()
    sys = lsys(axiom,rules,n)
    pos = []
    for cel in sys:
        if cel == "F":
            turtle.forward(l)
        elif cel == "+":
            turtle.right(angle)
        elif cel == "-":
            turtle.left(angle)
        elif cel == "[":
            pos.append(turtle.pos())
        elif cel == "]":
            new_pos = pos.pop()
            turtle.penup()
            turtle.setposition(new_pos)
            turtle.pendown()
dessin(5,3.1416/6,100)
