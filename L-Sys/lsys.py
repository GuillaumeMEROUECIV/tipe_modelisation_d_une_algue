def axiom():
    return "A"

def rules(str):
    if str == "A":
        return "AB"
    elif str == "B":
        return "A"
    else:
        return str

def next_gen(sys):
    next_gen = ""
    for cel in sys:
        next_gen += rules(cel)
    return next_gen

def lsys(axiom,rules,n):
    sys = axiom()
    for i in range(n):
        sys = next_gen(sys)
        print(sys)
    return sys

lsys(axiom,rules,5)
