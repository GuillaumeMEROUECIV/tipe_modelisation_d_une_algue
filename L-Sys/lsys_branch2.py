import turtle
def axiom():
    return "F"

def rules(str):
    if str == "F":
        return "F[-F]F[+F]F"
    else:
        return str

def next_gen(sys):
    next_gen = ""
    for cel in sys:
        next_gen += rules(cel)
    return next_gen

def lsys(axiom,rules,n):
    sys = axiom()
    for i in range(n):
        sys = next_gen(sys)
        print(sys)
    return sys

def dessin(n,angle,l):
    l = l/n
    turtle.reset()
    turtle.tracer(n=10000, delay=None)
    turtle.hideturtle()
    turtle.penup()
    turtle.setposition(-800,-500)
    turtle.left(35)
    turtle.pendown()
    sys = lsys(axiom,rules,n)
    pos = []
    for cel in sys:
        if cel == "F":
            turtle.forward(l)
        elif cel == "+":
            turtle.right(angle)
        elif cel == "-":
            turtle.left(angle)
        elif cel == "[":
            pos.append((turtle.pos(),turtle.heading()))
        elif cel == "]":
            (old_pos,old_angle) = pos.pop()
            turtle.penup()
            turtle.setposition(old_pos)
            turtle.setheading(old_angle)
            turtle.pendown()
turtle.speed(0)
dessin(5,25,50)
turtle.update()
turtle.mainloop()
