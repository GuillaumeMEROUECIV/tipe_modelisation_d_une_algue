import turtle
from random import randint
def axiom():
    return "X"

def rules(str):
    if str == "F":
        return "FF"
    elif str == "X":
        if randint(0,1) < 1:
            return "F-[[X]+X]+F[+FX]-X"
        else:
            return "F[−X]F[−FX]+X"
    else:
        return str

def next_gen(sys):
    next_gen = ""
    for cel in sys:
        next_gen += rules(cel)
    return next_gen

def lsys(axiom,rules,n):
    sys = axiom()
    for i in range(n):
        sys = next_gen(sys)
    return sys

def dessin(n,angle,l):
    turtle.reset()
    turtle.tracer(n=10000, delay=None)
    turtle.hideturtle()
    turtle.penup()
    turtle.setposition(0,-500)
    turtle.left(90)
    turtle.pendown()
    sys = lsys(axiom,rules,n)
    pos = []
    for cel in sys:
        if cel == "F":
            turtle.forward(l)
        elif cel == "+":
            turtle.right(angle_partiel)
        elif cel == "-":
            turtle.left(angle_partiel)
        elif cel == "[":
            pos.append((turtle.pos(),turtle.heading()))
        elif cel == "]":
            (old_pos,old_angle) = pos.pop()
            turtle.penup()
            turtle.setposition(old_pos)
            turtle.setheading(old_angle)
            turtle.pendown()
turtle.speed(0)
dessin(7,25,3)
turtle.update()
turtle.mainloop()
