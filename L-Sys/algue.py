import turtle
from random import randint
import colorsys
turtle.colormode(255)
def axiom():
    return "X"

def rules(str):
    if str == "F":
        return "FF"
    elif str == "X":
        return "F+F-F-F+F+FFF-F+F-F+FF-F+[++Y][----Y][+Y][Y][-Y][--Y][---Y]"
    elif str =="Y":
        return "F+F-[+Y]F-F+F[-Y]+F[Y]FF-F+F-F[+Y]+F-F[-Y]"
    else:
        return str

def next_gen(sys):
    next_gen = ""
    for cel in sys:
        next_gen += rules(cel)
    return next_gen

def lsys(axiom,rules,n):
    sys = axiom()
    for i in range(n):
        sys = next_gen(sys)
        print(sys)
    return sys

def dessin(n,angle,l):
    l = l/n
    turtle.reset()
    turtle.bgpic("bg2.gif")
    turtle.tracer(n=100000, delay=None)
    turtle.hideturtle()
    turtle.penup()
    turtle.setposition(0,-400)
    turtle.left(90)
    couleur = randint(120,255),randint(0,70),randint(0,70)
    turtle.color(couleur)
    turtle.pendown()
    sys = lsys(axiom,rules,n)
    pos = []
    for cel in sys:
        if cel == "F":
            turtle.forward(l)
        elif cel == "+":
            turtle.right(angle)
        elif cel == "-":
            turtle.left(angle)
        elif cel == "[":
            pos.append((turtle.pos(),turtle.heading()))
        elif cel == "]":
            (old_pos,old_angle) = pos.pop()
            turtle.penup()
            r = couleur[0] + int (randint(-100,100) / 93)
            g = couleur[1] + int (randint(-100,100) / 93)
            b = couleur[2] + int (randint(-100,100) / 93)
            if r < 0:
                r = 0
            if r > 255:
                r = 255
            if g < 0:
                g = 0
            if g > 255:
                g = 255
            if b < 0:
                b = 0
            if b > 255:
                b = 255
            couleur = (r,g,b)
            turtle.color(couleur)
            turtle.setposition(old_pos)
            turtle.setheading(old_angle)
            turtle.pendown()
turtle.speed(0)
dessin(5,20,7)
turtle.update()
turtle.mainloop()
