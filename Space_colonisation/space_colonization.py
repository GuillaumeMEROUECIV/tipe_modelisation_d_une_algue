import random as rd
from math import sqrt
from collections import defaultdict

D_supp = 20
R = 10


def plan(n,l,L):
    points = []
    for i in range(n):
        x,y = rd.randint(0,l),rd.randint(0,L)
        points.append((x,y))
    return points


def distance(a,b):
    xa, ya = a
    xb, yb = b
    return sqrt( (xb-xa)**2 + (yb-ya)**2 )

def plus_proche(point,ensemble):
    dmin = 1000000000 # max
    indice_min = 0
    for indice_point,coord_point in enumerate(ensemble):
        if distance(point,coord_point) < dmin:
            dmin = distance(point,coord_point)
            indice_min = indice_point
    return indice_min


def noeuds_attires(noeuds,attracteurs):
    dict_noeuds_attires = defaultdict(list) # defaultdict : dictionnaire de list donc on peut diretement utiliser append
    # dictionnaire de liste: si un noeud n'est le point le plus proche d'aucun attracteur il n'apparait pas dans l'ensemble, sinon une liste d'attracteur lui est associé
    for indice_attract,coord_attract in enumerate(attracteurs): # pour chaque points dans attracteurs
        i_plus_proche = plus_proche(coord_attract,noeuds) # on recupère l'indice du point (noeud) de l'arbre le plus plus proche
        dict_noeuds_attires[i_plus_proche].append(coord_attract)
    return dict_noeuds_attires


p = plan(100,10,10)
# print(p)
print(noeuds_attires([(1,1),(2,2)],p))

def vecteur_attract(node,list_attract):
    n = len(list_attract)
    xn,yn = node
    unit_x = []
    unit_y = []
    for attract in list_attract:
        xa, ya = attract
        vx = xa-xn
        vy = ya-yn
        d = distance(node,attract)
        #on normalise
        ux = vx/d
        uy = vy/d
        unit_x.append(ux)
        unit_y.append(uy)
    moyenne_x = sum(unit_x)/n
    moyenne_y = sum(unit_y)/n
    return(moyenne_x,moyenne_y)



#def arbre(node):
#    arbre = [node]
#    for i in range(100):

def new_point(point_depart,vecteur_attract):
    # th de thales
    x1,y1 = point_depart[0], point_depart[1]
    dx,dy = vecteur_attract[0],vecteur_attract[1]
    norme = sqrt(dx**2 + dy**2) # norme du vecteur directeur
    x,y = 2*R*dx/d, d*r*dy/d #normalement d = 1 car moyenne de vecteur unitaire
    return(x,y)

def pousse(arbre,attracteurs):
    print(attracteurs)
    nouvelles_pousses = []
    for noeud_attire in noeuds_attires(arbre,attracteurs):
        print(noeud_attire)
        vecteur_attraction = vecteur_attract(noeud_attire[0],noeud_attire[1])
        nouveau_point = new_point(noeud_attire[0],vecteur_attraction)
        nouvelles_pousses.append(nouveau_point)
    return nouvelles_pousses

# etape f : supprimer les attracteurs trops proches
def supp_attract(nouvelles_pousses,attracteurs):
    a_supp = []
    for indice_attract,coord_attract in enumerate(attracteurs):
        for node in nouvelles_pousses:
            if distance(coord_attract,node) < D_supp:
                a_supp.append(indice_attract)
    a_supp.reverse()
    # en supprimer les indices dans l'ordre decroissant permet d'éviter les erreurs liées aux changements d'indices liées à la suppression d'un element d'indice inférieur
    for i in a_supp :
        del attracteurs[i]
    return attracteurs

def space_colonization(arbre,attracteurs):
    while len(attracteurs) > 20:
        nouvelles_pousses = pousse(arbre,attracteurs)
        supp_attract(arbre,attracteurs)
        arbre += nouvelles_pousses
    return arbre

# print(space_colonization([(0,0)],plan(100,10,10)))
