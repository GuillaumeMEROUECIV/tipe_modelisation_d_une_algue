Le fond joue énormément sur la détection de l'algue.
Pour un fond uni bleu l'algue n'est pas reconnue (coefficient de corrélation trop faible pour conclure ) alors qu'avec un fond marin on a:

3 ('coral_reef', 0.018196464) 3_floue ('coral_reef', 0.02653694)
4 ('coral_reef', 0.24896231) 4_floue ('coral_reef', 0.20312262)

Je pensais qu'appliquer un filtre pour flouter allait "lisser" les traits et donc améliorer la détection, mais l'image 4 en est un contre-example.
